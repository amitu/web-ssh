package com.junpeng.ssh.webssh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 项目的启动入口类
 *
 * @author Junpeng-Li
 * @date 2022-04-04 15:09:27
 */
@SpringBootApplication
public class WebSshApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebSshApplication.class, args);
    }

}
