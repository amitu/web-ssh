# web-ssh

#### 介绍
使用Java语言，基于SpringBoot、JSch、WebSocket、Xterm.js开发的web端SSH连接工具。

#### 软件架构
后端使用SpringBoot框架，SSH终端连接使用的JSch，前后端通信使用的WebSocket。前端纯静态页面开发，SSH终端输入编辑器使用的Xterm.js。

#### 使用说明
1.  将源代码克隆到本地
2.  使用IDEA工具打开项目，下载Maven依赖后，在com.junpeng.ssh.webssh目录下找到WebSshApplication类启动即可。
3.  在浏览器输入localhost:8000 或 127.0.0.1:8000即可访问。

#### Docker安装
1.  将工程目录下的docker文件传输到含有docker服务的服务器上
2.  进入到docker/web-ssh目录下
3.  使用命令`docker build -t ssh:1.0.5 -f Dockerfile .`打包镜像
4.  使用命令`docker run -d -t --name ssh -p 8080:8080 ssh:1.0.5`启动
5.  使用浏览器输入: `http://服务器IP:8080`就可以访问了

#### 软件截图

![demo](./demo.png)
