const SETTING_ITEM_KEY = 'compile_setting'

/**
 * 获取撰写窗口设置
 * @returns {{mode: string, visible: boolean}|any}
 */
export function getSettings() {
    const setting = localStorage.getItem(SETTING_ITEM_KEY)
    if (!setting) {
        return {
            visible: false,
            mode: 'single'
        }
    }

    return JSON.parse(setting)
}

/**
 * 设置撰写窗口
 * @param key
 * @param value
 */
export function setSettings(key, value) {
    const setting = getSettings()
    setting[key] = value
    localStorage.setItem(SETTING_ITEM_KEY, JSON.stringify(setting))
}