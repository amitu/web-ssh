const API_KEY = 'fast-cmds', SETTING_KEY = 'fast-setting'

import {v1 as uuidv1} from 'uuid'

/**
 * 获取快速命令列表
 * @returns {{}}
 */
export function findAll() {
    const cmds = localStorage.getItem(API_KEY)
    if (!cmds) {
        return {}
    }

    return JSON.parse(cmds)
}

/**
 * 根据ID获取一条快速命令
 * @param id
 * @returns {*}
 */
export function getById(id) {
    return findAll()[id]
}

/**
 * 添加快速命令
 * @param data
 */
export function addCmd(data) {
    const cmds = findAll()

    data.id = uuidv1()
    cmds[data.id] = data

    localStorage.setItem(API_KEY, JSON.stringify(cmds))
}

/**
 * 修改快速命令
 * @param data
 */
export function editCmd(data) {
    const cmds = findAll()
    cmds[data.id] = data
    localStorage.setItem(API_KEY, JSON.stringify(cmds))
}

/**
 * 删除快速命令
 * @param cmdId
 */
export function removeCmd(cmdId) {
    const cmds = findAll()
    delete cmds[cmdId]
    localStorage.setItem(API_KEY, JSON.stringify(cmds))
}

/**
 * 获取设置
 * @returns {{mode: string, visible: boolean}|any}
 */
export function getStting() {
    const setting = localStorage.getItem(SETTING_KEY)
    if (!setting) {
        return {
            mode: 'single',
            visible: false
        }
    }

    return JSON.parse(setting)
}

/**
 * 添加设置
 * @param key
 * @param value
 */
export function setSetting(key, value) {
    const setting = getStting()
    setting[key] = value
    localStorage.setItem(SETTING_KEY, JSON.stringify(setting))
}
