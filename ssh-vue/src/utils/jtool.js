/**
 * 将对象转成数组
 *
 * 比如：
 * obj = {a: 1, b: 2, c: 3}
 *
 * 返回的array = [1, 2, 3]
 *
 * @param obj
 * @returns {[]}
 */
export function object2Array(obj) {
    const array = []

    Object.keys(obj).forEach(key => {
        array.push(obj[key])
    })

    return array
}