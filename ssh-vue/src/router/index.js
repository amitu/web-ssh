import Vue from 'vue'
import Router from 'vue-router'

import Ssh from '../views/ssh/index'

Vue.use(Router)

export const routerMap = [
    {
        path: '/',
        name: 'ssh',
        component: Ssh,
    }
]

export default new Router({
    mode: 'history',
    scrollBehavior: () => ({y: 0}),
    routes: routerMap
})
